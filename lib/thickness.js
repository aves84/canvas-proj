module.exports = {
    '0.8': 1,
    '1.0': 1.3,
    '1.2': 1.7,
    '1.5': 2.0,
    '1.8': 2.0,
    '2.0': 2.7,
    '2.5': 3.3,
    '3.0': 3.3,
    '3.5': 5.0,
    '4.0': 5.0,
    '4.5': 6.7,
    '5.0': 6.7,
    '6.0': 6.7,
};
