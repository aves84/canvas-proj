var $ = require('jquery');
var grid = require('./grid');
var drawer = require('./drawer');
var interface = require('./interface');
var angle = require('./angle');
var styles = require('./styles');
var storage = require('./storage');
var detail = require('./detail');
var coords = require('./coords');

var animate = window.requestAnimationFrame || window.webkitRequestAnimationFrame || function(fn) {
    setTimeout(fn, 40);
};
var app = {
    scales: [1, 2, 5, 10, 20, 50, 100],
    scale: 10,
    zoom: 15,
    center: [0, 0],
    cursor: [],
    snap: true,
    gridSize: 20
};

var Detail = detail(app);
var a2s, s2a, $p, $l, $a;

app.styles = styles;
app.grid = grid(app);
app.angle = angle(app);
app.storage = storage(app);
app.coords = coords(app);

app.activeDetail = new Detail();

function init() {
    app.$fg = $('#canvasForeground');
    app.$canvas = $('#canvas');
    app.ctx = app.$canvas[0].getContext('2d');

    app.w = app.ctx.canvas.width;
    app.h = app.ctx.canvas.height;
    app.drw = drawer(app.ctx);
    app.interface = interface(app);
    app.interface.composeDetailesList();
    app.interface.$popup.$length = app.interface.$popup.find('[name=can-length]');
    app.interface.$popup.$angle = app.interface.$popup.find('[name=can-angle]');

    a2s = app.coords.abs2screen;
    s2a = app.coords.screen2abs;
    $p = app.interface.$popup;
    $l = app.interface.$popup.$length;
    $a = app.interface.$popup.$angle;

    draw();

    app.$fg.on('wheel', function(e) {
        var s = app.scale;
        var d = -e.originalEvent.deltaY;
        d /= Math.abs(d);
        app.zoom += d;
        app.zoom = Math.min(30, Math.max(0, app.zoom));
        app.scale = app.scales[Math.round(app.zoom / 5)];
        if (s != app.scale) {
            var x = app.cursor[0];
            var y = app.cursor[1];
            app.cursor = app.coords.screen2abs([e.originalEvent.offsetX, e.originalEvent.offsetY]);
            app.center[0] += app.cursor[0] - x;
            app.center[1] += app.cursor[1] - y;
        }
        app.interface.$scaler.val(app.scale);

        animate(draw);
        return false;
    });
    app.$fg.on('mousemove', function(e) {
        app.cursor = app.coords.screen2abs([e.offsetX, e.offsetY]); // преобразуем экранные координаты в абсолютные
        app.activeDetail.checkHovering(app.cursor);
        app.coords.snap(app.cursor);
        if (app.mousePressed) {
            if (app.draggingPoint || app.activePoint && app.activePoint == app.hoveredPoint) {
                app.draggingPoint = app.activePoint;
                app.draggingPoint[0] = app.cursor[0];
                app.draggingPoint[1] = app.cursor[1];
            } else {
                app.center[0] += e.originalEvent.movementX / app.scale;
                app.center[1] += e.originalEvent.movementY / app.scale;
            }
        }
        animate(draw);
    });
    app.$fg.on('mouseout', function(e) {
        app.mousePressed = false;
    });
    app.$fg.on('mousedown', function(e) {
        if (e.which == 1) app.mousePressed = [e.offsetX, e.offsetY];
    });
    app.$fg.on('mouseup', function(e) {
        if (e.which == 1) {
            if (app.mousePressed) {
                var d = app.coords.dist([e.offsetX, e.offsetY], app.mousePressed);
                if (d < 5) { // кнопка отпущена рядом с местом, где нажата
                    if (!app.activePoint && !app.hoveredPoint && !app.activeDetail.path.length) { // выбранной точки нет, клик на пустом месте, точек в пути нет
                        app.activePath = app.activeDetail.path;
                        app.activePoint = [app.cursor[0], app.cursor[1]];
                        app.activePath.push(app.activePoint);
                    } else if (!app.activePoint && app.hoveredPoint) { // выбранной точки нет, клик на существующей точке, выбираем её
                        app.activePath = app.activeDetail.path;
                        app.activePoint = app.hoveredPoint;
                    } else if (app.activePoint && !app.hoveredPoint && !app.editPoint) { // есть выбранная точка
                        if (app.activePoint == app.activePath[app.activePath.length - 1]) { // если в конце пути, вставляем новую после неё
                            app.activePoint = [app.cursor[0], app.cursor[1]];
                            app.activePath.push(app.activePoint);
                        } else if (app.activePoint == app.activePath[0]) { // если в начале пути, вставляем новую перед ней
                            app.activePoint = [app.cursor[0], app.cursor[1]];
                            app.activePath.unshift(app.activePoint);
                        } else if (app.hoveredPoint) {
                            app.activePath = app.hoveredPath;
                            app.activePoint = app.hoveredPoint;
                        }
                    }
                }
            }
        } else if (e.which == 3) {
            if (app.hoveredPoint && app.activePoint == app.hoveredPoint) {
                app.activeDetail.deletePoint(app.hoveredPoint);
            }
            cancel();
        }
        app.mousePressed = false;
        app.draggingPoint = null;
        animate(draw);

    });
    app.$fg.on('contextmenu', function() { return false });
    $(document).on('keyup', function(e) {
        if (e.which == 27) {
            cancel();
            animate(draw);
        }
        if (e.which == 13) {
            if (app.editPoint) { // есть редактируемая точка
                setValue();
                app.interface.$popup.insertBefore(app.interface.$fg);
            } else if (app.activePoint) { // есть выбранная точка
                app.editPoint = app.activePoint;
                app.interface.$popup.insertAfter(app.interface.$fg);
            }
            animate(draw);
        }
    });
    app.interface.$popup.on('input', function(e) {
        setValue(true);
    });
    return app;
}

function cancel() {
    app.preEdit = false;
    app.editPoint = null;
    app.activePoint = null;
    app.activePath = null;
    app.interface.$popup.insertAfter(app.interface.$fg);
}

var draw = app.draw = function() {
    app.ctx.clearRect(0, 0, app.w, app.h);
    app.grid();
    app.activeDetail.draw();
    app.drw.point(a2s(app.cursor), 5, { fillStyle: 'gold' });

    var p, i, c, A, O, B, a1, a2, a, l;
    if (app.activePoint) { // есть активная точка
        p = a2s(app.activePoint); // её экранные координаты
        app.drw.box(p, 15, { strokeStyle: 'red' }); // рамка

        if (!app.editPoint) { // редактирование цифрами неактивно
            i = app.activePath.indexOf(app.activePoint);
            if (i == app.activePath.length - 1) {
                A = app.activePath[i - 1] || app.activePoint;
            } else if (i === 0) {
                A = app.activePath[i + 1];
            }
            if (A) {
                c = a2s(app.cursor);
                a = app.angle(A, app.activePoint, app.cursor);
                if (a > Math.PI) a = 2 * Math.PI - a;
                a = Math.round((a > Math.PI ? a = 2 * Math.PI - a : a) * 180 / Math.PI * 100) / 100;
                l = Math.round(app.coords.dist(app.cursor, app.activePoint) * 100) / 20; // TODO: масштаб
                app.drw.line(p, c, { strokeStyle: 'grey' });
                $p.css({
                    left: c[0] + 10,
                    top: c[1] - 50
                }).show();
                $l.prop('disabled', true).val(l);
                $a.prop('disabled', true).val(a);
            }
        } else { // редактирование цифрами
            i = app.activePath.indexOf(app.editPoint);
            if (app.activePath[i - 1]) {
                c = a2s(app.editPoint);
                O = app.activePath[i - 1];
                A = app.activePath[i - 2] || O;
                B = app.editPoint;
                a1 = Math.atan2(A[1] - O[1], A[0] - O[0]);
                a2 = Math.atan2(B[1] - O[1], B[0] - O[0]);
                a = a2 - a1;
                if (a > Math.PI) a -= 2 * Math.PI;
                if (a < -Math.PI) a += 2 * Math.PI;
                a = Math.round(a * 180 / Math.PI * 100) / 100;
                l = Math.round(app.coords.dist(app.editPoint, O) * 100) / 20; // TODO: масштаб
                if (!app.preEdit) {
                    $p.css({
                        left: c[0] + 10,
                        top: c[1] - 50
                    }).show();
                    $l.prop('disabled', false).val(l).focus();
                    $a.prop('disabled', false).val(a);
                } else {
                    app.drw.line(a2s(app.preEdit[0]), a2s(app.preEdit[1]), { strokeStyle: 'grey' });
                    app.drw.box(a2s(app.preEdit[1]), 10, { strokeStyle: 'blue' });
                }
            } else {
                app.editPoint = null;
            }
        }
    } else {
        $p.hide();
    }
    // DEBUG: рамка вокруг расчётных точек
    if (app.activePath && app.editPoint) {
        var ei = app.activePath.indexOf(app.editPoint);
        var eo = app.activePath[i - 1] || app.editPoint;
        var ea = app.activePath[i - 2] || eo;
        app.drw.box(a2s(eo), 15, { strokeStyle: 'magenta', lineWidth: 3 });
        app.drw.box(a2s(ea), 20, { strokeStyle: 'purple', lineWidth: 3 });
    }
};

var setValue = app.setValue = function(preEdit) {
    var i = app.activePath.indexOf(app.editPoint);
    var l = $l.val() / 5; // TODO: масштаб
    var a = $a.val() / 180 * Math.PI;
    var O = app.activePath[i - 1] || app.editPoint;
    var A = app.activePath[i - 2] || O;
    var a0 = app.angle(A, O, O);
    var x = O[0] + l * Math.cos(a0 + a);
    var y = O[1] + l * Math.sin(a0 + a);
    if (preEdit) {
        app.preEdit = [O, [x, y]];
    } else {
        app.preEdit = false;
        app.editPoint[0] = x;
        app.editPoint[1] = y;
        app.editPoint = null;
        $l.prop('disabled', true);
        $a.prop('disabled', true);
    }
    animate(draw);
};

module.exports = init;
