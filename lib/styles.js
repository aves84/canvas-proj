module.exports = {
    angles: {
        arc: { strokeStyle: 'green', lineWidth: 3 },
        text: { fillStyle: 'red', font: 'italic 14px serif' }
    },
    linePoints: { radius: 4, color: 'blue' },
    lines: { color: 'black', width: 2, textColor: 'darkblue' },
    linesText: { fillStyle: 'blue', font: '14px serif', }
};
