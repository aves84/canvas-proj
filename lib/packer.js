var $ = require('jquery');
var sort = {
    random  : function(a, b) { return Math.random() - 0.5 },
    w       : function(a, b) { return b.w - a.w },
    h       : function(a, b) { return b.h - a.h },
    a       : function(a, b) { return b.w * b.h - a.w * a.h },
    max     : function(a, b) { return Math.max(b.w, b.h) - Math.max(a.w, a.h) },
    min     : function(a, b) { return Math.min(b.w, b.h) - Math.min(a.w, a.h) },

    height  : function(a, b) { return sort.msort(a, b, ['h', 'w']) },
    width   : function(a, b) { return sort.msort(a, b, ['w', 'h']) },
    area    : function(a, b) { return sort.msort(a, b, ['a', 'h', 'w']) },
    maxside : function(a, b) { return sort.msort(a, b, ['max', 'min', 'h', 'w']) },

    msort   : function(a, b, criteria) { /* sort by multiple criteria */
        var diff, n;
        for (n = 0; n < criteria.length; n++) {
            diff = sort[criteria[n]](a, b);
            if (diff !== 0)
                return diff;
        }
        return 0;
    }
};

module.exports = function(allBlocks) {
    var $packing = $('#packing').empty();
    Object.keys(allBlocks).forEach(function(thickness) {
        var size = +thickness < 3 ? [1250, 2500] : [1500, 6000];
        var w = size[1] / 5;
        var h = size[0] / 5;
        var $title = $('<h4>').appendTo($packing);
        var blocks = allBlocks[thickness];
        var n = 0;
        while (blocks.length) {
            n++;
            var packer = new Packer(w, h);
            blocks.sort(sort.maxside);
            packer.fit(blocks);
            blocks = blocks.map(function(block) {
                if (!block.fit) {
                    var t = block.w;
                    block.w = block.h;
                    block.h = t;
                }
                return block;
            });
            blocks.sort(sort.maxside);
            packer.fit(blocks);
            var ctx = addCanvas(w, h);
            var fited = 0;
            blocks = blocks.filter(function(block) {
                if (block.fit) {
                    DrawRectangle(ctx, block.fit.x, block.fit.y, block.w, block.h);
                    fited++;
                } else {
                    var t = block.w;
                    block.w = block.h;
                    block.h = t;
                }
                return !block.fit;
            });
            if (!fited) {
                alert('Не удаётся вместить деталь на лист');
                break;
            }
        }
        $title.text('Толщина: ' + thickness + ' мм, листов: ' + n);
    });

    function addCanvas(w, h) {
        var canvas = $('<canvas>').attr({ 'width': w, height: h }).appendTo($packing)[0];
        var ctx = canvas.getContext('2d');
        return ctx;
    }

    function DrawRectangle(ctx, x, y, w, h) {
        ctx.fillStyle = ['red', 'blue', 'green', 'gold', 'pink'][Math.random() * 5 | 0];
        ctx.fillRect(x + 0.5, y + 0.5, w, h);
        ctx.strokeRect(x + 0.5, y + 0.5, w, h);
        ctx.fillStyle = 'black';
        ctx.fillText(w * 5 + 'x' + h * 5, x + 5, y + 15);
    }
};
