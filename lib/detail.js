var styles = require('./styles');

module.exports = function(app) {
    function draw(path) {
        var a2s = app.coords.abs2screen;
        var A, O, B, i = 0;
        while (i < path.length) {
            A = path[i - 1];
            O = path[i];
            B = path[i + 1];

            app.drw.point(app.coords.abs2screen(O), styles.linePoints.radius, { fillStyle: styles.linePoints.color });
            if (B) {
                app.drw.line(app.coords.abs2screen(O), app.coords.abs2screen(B), {
                    strokeStyle: styles.lines.color,
                    lineWidth: styles.lines.width
                });
                var dx = B[0] - O[0];
                var dy = B[1] - O[1];
                var d = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2)) * 5; // TODO: масштаб
                var coords = app.coords.abs2screen([B[0] - dx / 2, B[1] - dy / 2]);
                var options = Object.keys(styles.linesText)
                    .reduce(function(s, e) {
                        s[e] = styles.linesText[e];
                        return s;
                    }, {});
                options.translate = coords;
                options.rotate = Math.atan2(dy, dx);
                if (Math.abs(options.rotate) > Math.PI / 2) options.rotate += Math.PI;
                app.drw.text(['center', -10], Math.round(d * 100) / 100 + ' мм', options);
            }
            if (A && B) app.angle(a2s(A), a2s(O), a2s(B), Math.min(10 * app.scale, 50) / 2, styles.angles.arc);
            if (A) app.drw.point(app.coords.abs2screen(A), styles.linePoints.radius, { fillStyle: styles.linePoints.color });

            i++;
        }
    }

    function Detail(data) {
        if (!data) data = {};
        Object.keys(data).reduce(function(s, e) {
            s[e] = data[e];
            return s;
        }, this);
        if (!this.title) this.title = 'Новая деталь';
        if (!this.path) this.path = [];
    }
    Detail.prototype.draw = function() {
        draw(this.path);
    };
    Detail.prototype.checkHovering = function(coords) {
        var dist = app.coords.dist;
        var a2s = app.coords.abs2screen;
        app.hoveredPoint = this.path.reduce(function(s, e) {
            var d = dist(a2s(coords), a2s(e));
            return d < 10 ? e : s;
        }, null);
    };
    Detail.prototype.deletePoint = function(point) {
        this.path.splice(this.path.indexOf(point), 1);
    };

    return Detail;
};
