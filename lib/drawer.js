module.exports = function(ctx) {
    return {
        /**
         * Рисует линию между двумя точками
         * @param {Array}  start   [x, y] массив координат начальной точки
         * @param {Array}  end     [x, y] массив координат конечной точки
         * @param {Object} options объект свойств, применяемых к canvas
         */
        line: function(start, end, options) {
            ctx.save();
            this.applyOptions(options);
            ctx.beginPath();
            ctx.moveTo(start[0] + 0.5, start[1] + 0.5);
            ctx.lineTo(end[0] + 0.5, end[1] + 0.5);
            ctx.stroke();
            ctx.restore();
        },
        /**
         * Рисует прямоугольник из заданной точки указанной ширины и высоты, обводит или заполняем  при наличии соответствующих свойств в опциях
         * @param {Array}  start   [x, y] массив координат начальной точки
         * @param {Number} w       ширина
         * @param {Number} h       высота
         * @param {Object} options объект свойств, применяемых к canvas
         */
        rect: function(start, w, h, options) {
            ctx.save();
            this.applyOptions(options);
            if ('fillStyle' in options) {
                ctx.fillRect(start[0], start[1], w, h);
            }
            if ('strokeStyle' in options) {
                ctx.strokeRect(start[0], start[1], w, h);
            }
            ctx.restore();
        },
        /**
         * Рисует дугу
         * @param {Array}   c         [x, y] массив координат центра дуги
         * @param {Number}  r         радиус
         * @param {Number}  start     начальный угол в радианах
         * @param {Number}  end       конечний угол в радианах
         * @param {Boolean} clockwise направление рисования
         * @param {Object}  options   объект свойств, применяемых к canvas
         */
        arc: function(c, r, start, end, clockwise, options) {
            ctx.save();
            this.applyOptions(options);
            ctx.beginPath();
            ctx.arc(c[0], c[1], r, start, end, !clockwise);
            ctx.stroke();
            ctx.restore();
        },
        /**
         * Рисует точку, закрашенную окружность
         * @param {Array}  c       [x, y] массив координат центра
         * @param {Number} r       радиус окружности
         * @param {Object} options объект свойств, применяемых к canvas
         */
        point: function(c, r, options) {
            ctx.save();
            this.applyOptions(options);
            ctx.beginPath();
            ctx.arc(c[0], c[1], r, 0, 2 * Math.PI);
            ctx.fill();
            ctx.restore();
        },
        /**
         * Рисует надпись
         * @param   {Array}    c       [x, y] массив координат нижнего левого угла надписи
         * @param   {String}   str     строка текста
         * @param   {Object}   options объект свойств, применяемых к canvas
         */
        text: function(c, str, options) {
            ctx.save();
            if (typeof options != 'object') options = {};
            if ('translate' in options) ctx.translate(options.translate[0], options.translate[1]);
            if ('rotate' in options) ctx.rotate(options.rotate);
            var opts = Object.keys(options)
                .reduce(function(s, e) {
                    if (e != 'translate' && e != 'rotate') s[e] = options[e];
                    return s;
                }, {});
            this.applyOptions(opts);
            if (c[0] == 'center') c[0] = -ctx.measureText(str)
                .width / 2;
            ctx.fillText(str, c[0], c[1]);
            ctx.restore();
        },
        /**
         * Рисует рамку вокруг точки
         * @param {Array}  c       [x, y] массив координат точки
         * @param {Number} s       сторона квадрата рамки
         * @param {Object} options объект свойств, применяемых к canvas
         */
        box: function(c, s, options) {
            ctx.save();
            this.applyOptions(options);
            ctx.beginPath();
            var x = Math.round(c[0] - s / 2) - 0.5;
            var y = Math.round(c[1] - s / 2) - 0.5;
            ctx.moveTo(x, y);
            ctx.lineTo(x + 0.3 * s, y);
            ctx.moveTo(x + 0.7 * s, y);
            ctx.lineTo(x + s, y);
            ctx.lineTo(x + s, y + 0.3 * s);
            ctx.moveTo(x + s, y + 0.7 * s);
            ctx.lineTo(x + s, y + s);
            ctx.lineTo(x + 0.7 * s, y + s);
            ctx.moveTo(x + 0.3 * s, y + s);
            ctx.lineTo(x, y + s);
            ctx.lineTo(x, y + 0.7 * s);
            ctx.moveTo(x, y + 0.3 * s);
            ctx.lineTo(x, y - 1);
            ctx.stroke();
            ctx.restore();
        },
        /**
         * Применяет свойства к canvas
         * @param {Object} options объект свойств
         */
        applyOptions: function(options) {
            if (typeof options == 'object') Object.keys(options)
                .forEach(function(e) {
                    if (typeof ctx[e] == 'function') ctx[e](options[e]);
                    else ctx[e] = options[e];
                });
        }
    };
};
