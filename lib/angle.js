/**
 * Вычисляет угол вектора и приводит его к положительному значению.
 * @param   {Array}  c [x, y] массив координат начала вектора
 * @param   {Array}  l [x, y] массив координат конца вектора
 * @returns {Number} угол в радианах от 0 до 2*PI
 */
function angle(c, l) {
    var a = Math.atan2(l[1] - c[1], l[0] - c[0]);
    if (a < 0) a += Math.PI * 2;
    return a;
}

module.exports = function(app) {
    /**
     * Функция рисования угла между двумя линиями. Рисует меньший угол и надпись с его значением в градусах.
     * @param   {Array}  A,O,B   [x, y] массивы координат начальной, центральной и конечной точек угла,
     * @param   {Number} [r]       масстояние от центральной точки, на котором рисуется дуга угла, если не указан, функция только возврачает значение угла
     * @param   {Object} [options] объект свойств, применяемых к canvas
     * @returns {Number} значение нарисованного угла в градусах
     */
    return function(A, O, B, r, options) {
        if (!O || !A || !B) return NaN;
        var drw = app.drw;
        var ca1 = angle(O, A);
        var ca2 = angle(O, B);
        var a1 = Math.min(ca1, ca2);
        var a2 = Math.max(ca1, ca2);
        var da = a2 - a1;

        var x = O[0] + 20;
        var y = O[1] + 20;

        if (r > 0) {
            drw.arc(O, r, a1, a2, da < Math.PI, options);
            drw.text([x, y], Math.round((da < Math.PI ? da : 2 * Math.PI - da) * 180 / Math.PI) + '°', app.styles.angles.text);
        }

        return da;
    };
};
