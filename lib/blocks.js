var thicknessList = require('./thickness');

module.exports = function(app) {
    var dist = app.coords.dist;
    var angle = app.angle;

    function length(detail) {
        var path = detail.path;
        var r = thicknessList[detail.thickness];
        return Math.round(path.reduce(function(s, O, i) {
            var A = path[i - 1];
            var B = path[i + 1];
            var l1 = dist(O, A) * 5; // TODO: масштаб
            var l2 = dist(O, B) * 5;
            if (!l1 && l2) return l2;
            else if (l1 && !l2) return s;
            else {
                return s + l2 - 2 * r + r * angle(A, O, B);
            }
        }, 0));
    }

    var blocks = app.storage.reduce(function(blocks, detail) {
        var h = length(detail);
        var w = +detail.width;
        var t = detail.thickness;
        if (!(t in blocks)) blocks[t] = [];
        var i = 0;
        while (i++ < detail.count) blocks[t].push({w: w/5, h: h/5, t: t});
        return blocks;
    }, {});

    return blocks;
};
