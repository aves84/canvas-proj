var $ = require('jquery');
var detail = require('./detail');
var thinenessList = require('./thickness');

var popup = '<div id="can-popup"><label><input name="can-length" type="number">мм</label><br><label><input name="can-angle" type="number">°</label></div>';
var popupStyles = { display: 'none', position: 'absolute' };

module.exports = function(app) {
    var Detail = detail(app);

    $('#detailParams select[data-prop="thickness"]').html(Object.keys(thinenessList).reduce(function(s, e) {
        return s + '<option value="' + e + '">' + e + ' мм</option>';
    }, ''));

    $('#can-but-new').click(function() {
        app.activeDetail = new Detail();
        app.draw();
    });
    $('#can-but-save').click(function() {
        if (app.storage.indexOf(app.activeDetail) == -1) {
            app.storage.push(app.activeDetail);
        }
        $('#detailParams [data-prop]').each(function(i, e) {
            var prop = e.dataset.prop;
            var val = e.value;
            if (val) app.activeDetail[prop] = val;
        });
        app.storage.save();
        app.interface.composeDetailesList();

    });
    $('#can-but-delete').click(function(e) {
        var i = app.storage.indexOf(app.activeDetail);
        if (i != -1) {
            app.storage.splice(i, 1);
            app.storage.save();
            app.interface.composeDetailesList();
            app.activeDetail = new Detail();
            app.draw();
        }
    });
    $('#snaper input')
        .change(function(e) {
            app.snap = e.target.checked;
        })
        .prop('checked', true);

    return {
        $scaler: $('#scaler input')
            .change(function(e) {
                var v = e.target.value;
                if (v < 1) v = 1;
                var a = app.scales.map(function(e) {
                    return Math.abs(1 - v / e);
                });
                var min = 0;
                var i = a.length;
                while (i--) {
                    if (a[i] < a[min]) min = i;
                }
                e.target.value = app.scale = app.scales[min];
                app.zoom = min * 5;
                app.draw();
            })
            .val(app.scale),
        $storage: $('#block-prof')
            .click(function(e) {
                if (e.target.tagName == 'LI') {
                    app.storage.reload();
                    var d = app.activeDetail = app.storage[$(e.target).index()];
                    var p = $('#detailParams');
                    Object.keys(d).forEach(function(e) {
                        p.find('[data-prop="' + e + '"]').val(d[e]);
                    });
                    app.activePath = app.activePoint = null;
                    app.draw();
                }
            }),
        $packing: $('#packing'),
        $popup: $(popup)
            .css(popupStyles)
            .prependTo($('#canvasWrapper')),
        composeDetailesList: function() {
            var items = app.storage.map(function(e) {
                return $('<li/>').text(e.title);
            });
            app.interface.$storage.html(items);
        }
    };
};
