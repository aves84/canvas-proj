module.exports = function(app) {
    function screen2abs(coords) {
        return [(coords[0] - app.w / 2) / app.scale - app.center[0],
            (coords[1] - app.h / 2) / app.scale - app.center[1]];
    }

    function abs2screen(coords) {
        return [(coords[0] + app.center[0]) * app.scale + app.w / 2,
            (coords[1] + app.center[1]) * app.scale + app.h / 2];
    }

    function snap(coords) {
        if (app.snap) {
            var s = app.gridSize / app.scale;
            coords[0] = Math.round(coords[0] / s) * s;
            coords[1] = Math.round(coords[1] / s) * s;
        }
        return coords;
    }

    function dist(coords, target) {
        if (!coords || !target) return false;
        var dx = coords[0] - target[0];
        var dy = coords[1] - target[1];
        return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
    }

    return {
        screen2abs: screen2abs,
        abs2screen: abs2screen,
        snap: snap,
        dist: dist
    };
};
