/**
 * Модуль работы с хранилищем.
 * @param   {Object}  app объект приложения
 * @returns {Object}  массив с дополнительными методами save, add
 */

var detail = require('./detail');

function load() {
    try {
        return JSON.parse(localStorage.getItem('detailsData')) || [];
    } catch (e) {
        return [];
    }
}

module.exports = function(app) {
    var Detail = detail(app);
    var data = load().map(function(e) { return new Detail(e) });
    return Object.defineProperties(data, {
        /**
         * Сохраняет текущее состояние в localStorage.
         * @returns {Boolean} true при успехе, false при неудаче
         */
        save: {
            value: function() {
                try {
                    localStorage.setItem('detailsData', JSON.stringify(this));
                } catch (e) {
                    return false;
                }
                return true;
            }
        },
        reload: {
            value: function() {
                this.length = 0;
                var data = load().map(function(e) { return new Detail(e) });
                for (var i = 0; i < data.length; i++) this[i] = data[i];
            }
        }
    });
};
