module.exports = function(app) { // рисует сетку
    return function() {
        var drw = app.drw;
        var d = app.gridSize;
        var x = Math.round(app.center[0] * app.scale + app.w/2) % d - d;
        var y = Math.round(app.center[1] * app.scale + app.h/2) % d - d;
        var dash = [d / 4];
        var options = {
            strokeStyle: 'rgba(0,10,20,0.5)',
            setLineDash: dash,
        };

        while ((y += d) < app.h) {
            options.lineDashOffset = -Math.round(app.center[0] * app.scale) % d + dash[0] / 2;
            drw.line([0, y], [app.w, y], options);
        }
        while ((x += d) < app.w) {
            options.lineDashOffset = -Math.round(app.center[1] * app.scale) % d + dash[0] / 2;
            drw.line([x, 0], [x, app.h], options);
        }
        drw.point([app.center[0] * app.scale + app.w/2, app.center[1] * app.scale + app.h/2], 3, {fillStyle: 'red'});
    };
};
