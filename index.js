var $ = require('jquery');
var init = require('./lib/app');
var blocks = require('./lib/blocks');
var packer = require('./lib/packer');

$(function() {
    var app = init();
    $('#calculate').click(function() {
        packer(blocks(app), app.scale / 5); // TODO: масштаб
    });
});
